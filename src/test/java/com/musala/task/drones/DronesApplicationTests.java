package com.musala.task.drones;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.musala.task.drones.constants.Endpoints;
import com.musala.task.drones.constants.StatusCodes;
import com.musala.task.drones.dtos.DroneDto;
import com.musala.task.drones.dtos.LoadingDto;
import com.musala.task.drones.dtos.MedicationDto;
import com.musala.task.drones.models.Drone;
import com.musala.task.drones.models.Medication;
import com.musala.task.drones.repositories.DroneRepository;
import com.musala.task.drones.repositories.MedicationRepository;
import com.musala.task.drones.services.DroneService;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class DronesApplicationTests {

	@Autowired
	MockMvc mockMvc;

	@Autowired
	private DroneService droneService;

	@Autowired
	private MedicationRepository medicationRepository;

	@Autowired
	private DroneRepository droneRepository;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void whenValidInput_thenCreateDrone_thenCheckAvailableDroneFroLoading() throws Exception {
		String serialNumber = UUID.randomUUID().toString();
		registerDrone(serialNumber);

		mockMvc.perform(MockMvcRequestBuilders.get(Endpoints.AVAILABLE_DRONES_FOR_LOADING)
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.data", hasSize(greaterThanOrEqualTo(10))))
				.andExpect(jsonPath("$.data[10].serialNumber", is(serialNumber)));
	}

	@Test
	public void givenValidDroneAndMedication_thenLoadDroneWithMedication_thenGetLoadedMedication() throws Exception {
		String serialNumber = UUID.randomUUID().toString();
		registerDrone(serialNumber);

		LoadingDto loadingDto = LoadingDto.builder()
				.medications(getMedications())
				.droneSerialNumber(serialNumber)
				.build();

		String requestBody = toJson(loadingDto);

		mockMvc.perform(MockMvcRequestBuilders.post(Endpoints.LOAD_DRONE_WITH_MED)
				.contentType(MediaType.APPLICATION_JSON)
				.content(requestBody))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.statusCode", is(StatusCodes.SUCCESS)))
				.andExpect(jsonPath("$.data[0].drone.serialNumber", is(serialNumber)));

		mockMvc.perform(MockMvcRequestBuilders.get(Endpoints.DRONE_LOADED_MEDICATIONS, serialNumber)
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.data", hasSize(equalTo(4))));
	}

	@Test
	public void givenValidDrone_thenCheckBatteryLevel() throws Exception {
		String serialNumber = UUID.randomUUID().toString();
		registerDrone(serialNumber);

		Drone drone = droneRepository.findBySerialNumber(serialNumber).get();
		mockMvc.perform(MockMvcRequestBuilders.get(Endpoints.DRONE_BATTERY_LEVEL, serialNumber)
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.data[0].batteryLevel", is(drone.getBatteryCapacity())));
	}

	private void registerDrone(String droneSerialNumber) {
		Random r = new Random();
		double randomValue = 50 + (500 - 50) * r.nextDouble();
		DroneDto droneDto = DroneDto.builder()
				.serialNumber(droneSerialNumber)
				.weight(randomValue)
				.build();
		droneService.registerDrone(droneDto);
	}

	private List<MedicationDto> getMedications() {
		// create medications
		Medication xanax = createMedications("XAX_350", "XANAX", "http://image.com/xanax", 30);
		Medication ibuprofen = createMedications("IBF_1000", "IBUPROFEN", "http://image.com/ibuprofen", 20);
		Medication ducolax = createMedications("DLAX900", "DUCOLAX", "http://image.com/ducolax", 25);
		Medication rhobophenol = createMedications("RHOBO_C6H6O", "RHOBOPHENOL", "http://image.com/rhobophenol", 45);
		List<MedicationDto> meds = Arrays.asList(new MedicationDto(xanax.getCode()),
				new MedicationDto(ibuprofen.getCode()), new MedicationDto(ducolax.getCode()),
				new MedicationDto(rhobophenol.getCode()));
		return meds;
	}

	private Medication createMedications(String code, String name, String image, double weight) {
		Medication med = Medication.builder()
				.code(code)
				.image(image)
				.name(name)
				.weight(weight)
				.build();
		return medicationRepository.save(med);
	}

	private String toJson(Object obj) throws Exception {
		objectMapper = new ObjectMapper();
		objectMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = objectMapper.writer().withDefaultPrettyPrinter();
		return ow.writeValueAsString(obj);
	}
}
