CREATE TABLE IF NOT EXISTS drones (
    id serial NOT NULL PRIMARY KEY,
    serial_number character varying(100),
    model character varying(255),
    battery_capacity integer,
    state character varying(255),
    weight_limit character varying(10),
    CONSTRAINT uk_lsjvi6h63ga5x3qwvo5voq1f8 UNIQUE (serial_number)
);

CREATE TABLE IF NOT EXISTS medications (
    id serial NOT NULL PRIMARY KEY,
    code character varying(255),
    image character varying(255),
    name character varying(255),
    weight double precision,
    CONSTRAINT uk_ql2ubnmoxyc4xip8hfypisaq8 UNIQUE (code),
    CONSTRAINT uk_rnthlywygii0cyvlwdygqb9gy UNIQUE (name)
);

INSERT INTO drones(serial_number, model, battery_capacity, state, weight_limit) 
values ('b167274e-38db-4081-b021-8177545edb38', 'Lightweight', 100, 'IDLE', '125gr'),
('68893c38-aa14-4644-bd6b-605c6e8ab768', 'Lightweight', 20, 'IDLE', '125gr'),
('0abfe2b3-15b1-44d7-87d9-69cc68b55139', 'Middleweight', 95, 'IDLE', '250gr'),
('fd6baf25-2925-495c-b077-19cc86dd8815', 'Middleweight', 100, 'IDLE', '250gr'),
('76tyrf25-2925-495c-b077-19cc86ebj876', 'Middleweight', 100, 'IDLE', '250gr'),
('8777a1da-b9dc-4015-92bf-a6e9dd2906ee', 'Cruiserweight', 100, 'IDLE', '375gr'),
('6b347b27-acf1-42cb-ac75-f14603e9e47e', 'Cruiserweight', 80, 'IDLE', '375gr'),
('3af2b6b1-0e1e-4396-9e55-9916f040bf20', 'Cruiserweight', 20, 'IDLE', '375gr'),
('91f3cc73-0477-49cf-b965-a45f286ae2ff', 'Heavyweight', 10, 'IDLE', '500gr'),
('39c720ff-360b-4a94-b17b-d558eec71fab', 'Heavyweight', 69, 'IDLE', '500gr');

INSERT INTO medications (name, weight, code, image)
values ('Rialpixel', 70, 'ASDXFC56', 'http://placeimg.com/640/480'),
('panelmicrochip', 28, 'KJHG456', 'http://placeimg.com/640/480'),
('Cornersmicrochip', 70, 'RTE4577', 'http://placeimg.com/640/480'),
('Kronealarm', 35, 'JHG23454', 'http://placeimg.com/640/480'),
('Mobilitycard', 70, 'WERTY54', 'http://placeimg.com/640/480'),
('JBODpanel', 70, 'R45TRGH', 'http://placeimg.com/640/480'),
('Pantssensor', 90, 'GHFDH457', 'http://placeimg.com/640/480'),
('Cottonmonitor', 40, 'XZCVB57IU', 'http://placeimg.com/640/480'),
('Seniorpixel', 19, '5S4DTCYVH', 'http://placeimg.com/640/480'),
('AccountAvon', 63, 'WEXCGHVY7', 'http://placeimg.com/640/480'),
('mobileWyoming', 44, 'HRZE568G', 'http://placeimg.com/640/480'),
('FacilitatorBerkshire', 50, 'ZXCTVIU7', 'http://placeimg.com/640/480'),
('COMQuality', 20, 'RX54OUPI', 'http://placeimg.com/640/480'),
('Ariaryreboot', 60, 'BPVCUXF7', 'http://placeimg.com/640/480'),
('engageup', 20, 'ZX6CUVI0', 'http://placeimg.com/640/480'),
('Keyboardinfomediaries', 49, 'KJH76664', 'http://placeimg.com/640/480'),
('protocoloptical', 70, 'VXEZ56YU', 'http://placeimg.com/640/480');