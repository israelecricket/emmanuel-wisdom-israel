package com.musala.task.drones.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.musala.task.drones.models.Manifest;
import com.musala.task.drones.models.ManifestMedications;

public interface ManifestMedicationRepository extends JpaRepository<ManifestMedications, Long> {

    List<ManifestMedications> findByManifest(Manifest manifest);
}
