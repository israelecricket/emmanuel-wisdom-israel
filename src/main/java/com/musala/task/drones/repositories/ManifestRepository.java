package com.musala.task.drones.repositories;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.musala.task.drones.models.Drone;
import com.musala.task.drones.models.Manifest;

@Repository
public interface ManifestRepository extends JpaRepository<Manifest, UUID> {

    Optional<Manifest> findByDrone(Drone drone);
}
