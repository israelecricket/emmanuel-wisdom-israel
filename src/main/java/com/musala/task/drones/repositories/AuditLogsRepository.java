package com.musala.task.drones.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.musala.task.drones.models.AuditLogs;

@Repository
public interface AuditLogsRepository extends JpaRepository<AuditLogs, Long> {

}
