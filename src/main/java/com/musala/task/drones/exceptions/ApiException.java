package com.musala.task.drones.exceptions;

import java.time.ZonedDateTime;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ApiException {

    private String errorMessage;
    private String statusCode;
    private ZonedDateTime zonedDateTime;
}
