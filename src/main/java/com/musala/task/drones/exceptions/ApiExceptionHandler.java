package com.musala.task.drones.exceptions;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import com.musala.task.drones.constants.StatusCodes;
import com.musala.task.drones.dtos.ResponseDTO;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class ApiExceptionHandler {

    @Value("${minimum.battery.level.for.loading}")
    private String MINIMUM_BATTERY_LEVEL_FOR_LOADING;

    @ExceptionHandler(RecordNotFoundException.class)
    @ResponseBody
    public ResponseDTO<?> handleDroneNotFoundException(RecordNotFoundException e) {
        new ResponseDTO<>();
        return ResponseDTO.builder()
                .errors(Arrays.asList(e.getMessage()))
                .data(Collections.emptyList())
                .statusMessage("Record not found")
                .statusCode(StatusCodes.NOT_FOUND)
                .build();
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseBody
    public ResponseDTO<?> handleBadRequestException(BadRequestException e) {
        new ResponseDTO<>();
        return ResponseDTO.builder()
                .errors(Arrays.asList(e.getMessage()))
                .data(Collections.emptyList())
                .statusMessage("Bad Request")
                .statusCode(StatusCodes.BAD_REQUEST)
                .build();
    }

    @ExceptionHandler(DuplicateException.class)
    @ResponseBody
    public ResponseDTO<?> handleDuplicateRecordException(DuplicateException e) {
        new ResponseDTO<>();
        return ResponseDTO.builder()
                .errors(Arrays.asList(e.getMessage()))
                .data(Collections.emptyList())
                .statusMessage("Duplicate Record")
                .statusCode(StatusCodes.DUPLICATE)
                .build();
    }

    @ExceptionHandler(BatteryCapacityException.class)
    @ResponseBody
    public ResponseDTO<?> handleBatteryCapacityException(BatteryCapacityException e) {
        String msg = String.format("Minimum Battery Capacity for loading is %s%%",
                MINIMUM_BATTERY_LEVEL_FOR_LOADING);
        new ResponseDTO<>();
        return ResponseDTO.builder()
                .errors(Arrays.asList(e.getMessage()))
                .data(Collections.emptyList())
                .statusMessage(msg)
                .statusCode(StatusCodes.FAILED)
                .build();
    }

    @ExceptionHandler(DroneStateException.class)
    @ResponseBody
    public ResponseDTO<?> handleDroneStateException(DroneStateException e) {
        new ResponseDTO<>();
        return ResponseDTO.builder()
                .errors(Arrays.asList(e.getMessage()))
                .data(Collections.emptyList())
                .statusMessage("Only IDLE drones can be loaded")
                .statusCode(StatusCodes.FAILED)
                .build();
    }

    @ExceptionHandler(DroneWeightException.class)
    @ResponseBody
    public ResponseDTO<?> handleDroneWeightException(DroneWeightException e) {
        new ResponseDTO<>();
        return ResponseDTO.builder()
                .errors(Arrays.asList(e.getMessage()))
                .data(Collections.emptyList())
                .statusMessage("Drone max weight limit exceeded.")
                .statusCode(StatusCodes.FAILED)
                .build();
    }

    @ExceptionHandler
    public final ResponseDTO<?> handleGeneralExceptions(
            Exception ex,
            WebRequest request) {
        log.error("default exceptions being handled");
        log.trace(ex.getMessage());
        log.trace(ex.getMessage(), ex);

        // status message for this error
        String statusMessage = ex.getMessage();

        log.error(statusMessage);

        // build the response payload
        return new ResponseDTO<>(
                StatusCodes.FAILED,
                "error processing your request",
                Arrays.asList(),
                Arrays.asList(statusMessage));
    }
}
