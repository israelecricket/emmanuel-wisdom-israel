package com.musala.task.drones.exceptions;

import lombok.Getter;

@Getter
public class RecordNotFoundException extends RuntimeException {

    private final String message;

    public RecordNotFoundException(String message) {
        super(message);
        this.message = message;
    }
}
