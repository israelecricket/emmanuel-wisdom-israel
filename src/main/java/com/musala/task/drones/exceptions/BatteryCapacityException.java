package com.musala.task.drones.exceptions;

import lombok.Getter;

@Getter
public class BatteryCapacityException extends RuntimeException {

    private final String message;

    public BatteryCapacityException(String message) {
        super(message);
        this.message = message;
    }
}
