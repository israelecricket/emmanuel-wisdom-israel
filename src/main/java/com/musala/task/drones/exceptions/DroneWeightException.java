package com.musala.task.drones.exceptions;

import lombok.Getter;

@Getter
public class DroneWeightException extends RuntimeException {

    private final String message;

    public DroneWeightException(String message) {
        super(message);
        this.message = message;
    }
}
