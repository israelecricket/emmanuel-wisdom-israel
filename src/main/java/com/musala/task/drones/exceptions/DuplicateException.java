package com.musala.task.drones.exceptions;

import lombok.Getter;

@Getter
public class DuplicateException extends RuntimeException {

    private final String message;

    public DuplicateException(String message) {
        super(message);
        this.message = message;
    }
}
