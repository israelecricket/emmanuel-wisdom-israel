package com.musala.task.drones.exceptions;

import lombok.Getter;

@Getter
public class DroneStateException extends RuntimeException {

    private final String message;

    public DroneStateException(String message) {
        super(message);
        this.message = message;
    }
}
