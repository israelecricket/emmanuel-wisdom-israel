package com.musala.task.drones.controllers;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.musala.task.drones.constants.Endpoints;
import com.musala.task.drones.dtos.DroneDto;
import com.musala.task.drones.dtos.LoadingDto;
import com.musala.task.drones.dtos.ResponseDTO;
import com.musala.task.drones.services.DroneService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class DispatchController {

    private final DroneService droneService;

    @PostMapping(value = Endpoints.REGISTER_DRONE, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseDTO<?> getCustomerEligibleProducts(@Valid @RequestBody DroneDto droneDto) {
        return droneService.registerDrone(droneDto);
    }

    @PostMapping(value = Endpoints.LOAD_DRONE_WITH_MED, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseDTO<?> loadDroneWithMedications(
            @Valid @RequestBody LoadingDto loadingDto) {
        return droneService.loadDroneWithMedications(loadingDto);
    }

    @GetMapping(value = Endpoints.DRONE_LOADED_MEDICATIONS, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseDTO<?> getLoadedMedicationsForDrone(
            @PathVariable(name = "droneSerialNumber", required = true) String droneSerialNumber) {
        return droneService.checkLoadedMedicationForDrone(droneSerialNumber);
    }

    @GetMapping(value = Endpoints.AVAILABLE_DRONES_FOR_LOADING, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseDTO<?> getAvailableDronesForLoading() {
        return droneService.getAvailableDronesForLoading();
    }

    @GetMapping(value = Endpoints.DRONE_BATTERY_LEVEL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseDTO<?> getDroneBatteryLevel(
            @PathVariable(name = "droneSerialNumber", required = true) String droneSerialNumber) {
        return droneService.getDroneBatteryLevel(droneSerialNumber);
    }

    @GetMapping(value = Endpoints.LIST_OF_MEDICATIONS, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseDTO<?> fetchListOfMedications() {
        return droneService.getListOfMedications();
    }
}