package com.musala.task.drones.dtos;

import java.util.List;

import com.musala.task.drones.models.Drone;

import lombok.Data;

@Data
public class ManifestDto {

    private Long id;
    private Drone drone;
    private List<MedicationDto> medications;
}
