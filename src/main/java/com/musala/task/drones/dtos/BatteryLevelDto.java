package com.musala.task.drones.dtos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BatteryLevelDto {

    private Integer batteryLevel;
}
