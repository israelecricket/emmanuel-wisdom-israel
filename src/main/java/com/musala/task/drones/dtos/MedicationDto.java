package com.musala.task.drones.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicationDto {

    @NotBlank(message = "field code cannot be blank")
    @NotEmpty(message = "field code cannot be empty or null")
    private String code;
}
