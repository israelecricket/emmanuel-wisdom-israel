package com.musala.task.drones.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DroneDto {

    @NotBlank(message = "field serial number cannot be blank")
    @NotEmpty(message = "field serial number cannot be empty or null")
    @Length(max = 100, message = "allowed max length for field serial number is 100")
    private String serialNumber;

    @NotNull(message = "field weight cannot be null")
    private double weight;
}
