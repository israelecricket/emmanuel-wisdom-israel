package com.musala.task.drones.dtos;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LoadingDto {

    @NotNull
    private List<MedicationDto> medications;

    @NotNull
    @NotEmpty
    @NotBlank
    private String droneSerialNumber;
}
