package com.musala.task.drones.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "medications")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Medication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(unique = true)
    @Pattern(regexp = "[A-Za-z0-9_-]{2,}", message = "allowed only letters, numbers, '-', '_'")
    private String name;

    @Column(scale = 2)
    private double weight;

    @Column(unique = true)
    @Pattern(regexp = "[A-Z0-9_]{2,}", message = "allowed only upper case letters, underscore and numbers")
    private String code;

    private String image;
}
