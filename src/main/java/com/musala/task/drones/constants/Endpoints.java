package com.musala.task.drones.constants;

public class Endpoints {

    public static final String REGISTER_DRONE = "/drone/register";
    public static final String LIST_OF_DRONES = "/drones/list";
    public static final String LOAD_DRONE_WITH_MED = "/drone/meds/load";
    public static final String DRONE_LOADED_MEDICATIONS = "/drone/{droneSerialNumber}/meds";
    public static final String DRONE_BATTERY_LEVEL = "/drone/{droneSerialNumber}/battery-level";
    public static final String AVAILABLE_DRONES_FOR_LOADING = "/drones/list/loading";

    public static final String CREATE_MEDICATIONS = "/med/create";
    public static final String LIST_OF_MEDICATIONS = "/med/list";
}
