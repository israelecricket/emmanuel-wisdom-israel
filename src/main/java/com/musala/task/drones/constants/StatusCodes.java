package com.musala.task.drones.constants;

public class StatusCodes {

    private StatusCodes() {
    }

    public static final String SUCCESS = "0000";
    public static final String FAILED = "9000";
    public static final String NOT_FOUND = "9001";
    public static final String BAD_REQUEST = "4000";
    public static final String DUPLICATE = "6000";
}