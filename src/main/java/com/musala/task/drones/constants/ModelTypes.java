package com.musala.task.drones.constants;

public enum ModelTypes {
    Lightweight, Middleweight, Cruiserweight, Heavyweight
}