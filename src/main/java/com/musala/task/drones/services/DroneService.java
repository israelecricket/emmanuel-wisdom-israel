package com.musala.task.drones.services;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.musala.task.drones.constants.DroneState;
import com.musala.task.drones.constants.ModelTypes;
import com.musala.task.drones.constants.StatusCodes;
import com.musala.task.drones.dtos.BatteryLevelDto;
import com.musala.task.drones.dtos.DroneDto;
import com.musala.task.drones.dtos.LoadingDto;
import com.musala.task.drones.dtos.ManifestDto;
import com.musala.task.drones.dtos.MedicationDto;
import com.musala.task.drones.dtos.ResponseDTO;
import com.musala.task.drones.exceptions.BatteryCapacityException;
import com.musala.task.drones.exceptions.DroneStateException;
import com.musala.task.drones.exceptions.DroneWeightException;
import com.musala.task.drones.exceptions.DuplicateException;
import com.musala.task.drones.exceptions.RecordNotFoundException;
import com.musala.task.drones.models.AuditLogs;
import com.musala.task.drones.models.Drone;
import com.musala.task.drones.models.Manifest;
import com.musala.task.drones.models.ManifestMedications;
import com.musala.task.drones.models.Medication;
import com.musala.task.drones.repositories.AuditLogsRepository;
import com.musala.task.drones.repositories.DroneRepository;
import com.musala.task.drones.repositories.ManifestMedicationRepository;
import com.musala.task.drones.repositories.ManifestRepository;
import com.musala.task.drones.repositories.MedicationRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class DroneService {

    private final DroneRepository droneRepository;
    private final MedicationRepository medicationRepository;
    private final ManifestRepository manifestRepository;
    private final ManifestMedicationRepository manifestMedicationRepository;
    private final AuditLogsRepository auditLogsRepository;

    @Value("${minimum.battery.level.for.loading}")
    private String MINIMUM_BATTERY_LEVEL_FOR_LOADING;

    private int droneCount;
    private Manifest manifest;

    public ResponseDTO<?> registerDrone(DroneDto droneDto) {
        log.info("Register drone request - {}", droneDto);

        String serialNumber = droneDto.getSerialNumber();
        // CHECK FOR DUPLICATE WITH DRONE SERIAL NUMBER
        var drone = droneRepository.findBySerialNumber(serialNumber);
        if (drone.isEmpty()) {
            // no drone record exist with supplied serial number.
            // Proceed to create drone record
            ModelTypes model = determineDroneModel(droneDto.getWeight());
            Drone newDrone = Drone.builder()
                    .serialNumber(serialNumber)
                    .batteryCapacity(100)
                    .model(model)
                    .state(DroneState.IDLE)
                    .weightLimit(determineWeightLimit(model))
                    .build();
            droneRepository.save(newDrone);

            log.info("drone successfully registered drone - {}", newDrone);
            return new ResponseDTO<>(
                    StatusCodes.SUCCESS,
                    "successfully registered drone",
                    Arrays.asList(newDrone));
        }

        // throw duplicate exception
        log.info("duplicate record for drone with serial number - {}", serialNumber);
        throw new DuplicateException(
                String.format("Duplicate record! Drone with serial number %s already exist", serialNumber.toString()));
    }

    public ResponseDTO<?> loadDroneWithMedications(LoadingDto loadingDto) {
        String droneSerialNumber = loadingDto.getDroneSerialNumber();
        log.info("Drone Medications Loading Request - {}", loadingDto);
        // check if drone record exist
        var drone = droneRepository.findBySerialNumber(droneSerialNumber);
        if (drone.isEmpty()) {
            throw new RecordNotFoundException(String.format(
                    "No registered drone with serial number - %s. Register drone and try again", droneSerialNumber));
        }

        // check the battery level if its pass for loading
        if (!checkDroneBatteryCapacityForLoading(drone.get().getBatteryCapacity())) {
            throw new BatteryCapacityException("Drone Battery Capacity Level is too low for Loading");
        }

        // check state of the drone
        if (!checkDroneState(drone.get().getState())) {
            throw new DroneStateException("Invalid Drone state. Can only load IDLE drone");
        }

        // set drone state to loading
        updateDroneState(drone.get(), DroneState.LOADING);

        // check the total weight of medications to be loaded
        double droneWeightLimit = Double.valueOf(drone.get().getWeightLimit().replace("gr", ""));
        double totalMedWeight = calculateTotalMedicationWeight(loadingDto.getMedications());
        if (totalMedWeight > droneWeightLimit) {
            throw new DroneWeightException(
                    String.format("Drone with serial number -%s can only carry a max of %s", droneSerialNumber,
                            drone.get().getWeightLimit()));
        }

        manifest = Manifest.builder()
                .drone(drone.get())
                .build();
        manifest = manifestRepository.save(manifest);
        ManifestDto manifestDto = new ManifestDto();
        BeanUtils.copyProperties(manifest, manifestDto);
        manifestDto.setMedications(loadingDto.getMedications());

        List<String> medications = getMedications(loadingDto.getMedications());
        medications.forEach(med -> {
            manifestMedicationRepository.save(
                    ManifestMedications.builder()
                            .manifest(manifest)
                            .medication(med)
                            .build());
        });

        // update drone state to Loaded
        updateDroneState(drone.get(), DroneState.LOADED);

        return new ResponseDTO<>(
                StatusCodes.SUCCESS,
                "drone loaded successfully",
                Arrays.asList(manifestDto));
    }

    public ResponseDTO<?> checkLoadedMedicationForDrone(String droneSerialNumber) {
        log.info("Loaded Medications Items List for Drone - {}", droneSerialNumber);
        // check if drone record exist
        var drone = droneRepository.findBySerialNumber(droneSerialNumber);
        if (drone.isEmpty()) {
            throw new RecordNotFoundException(String.format(
                    "No registered drone with serial number - %s. Register drone and try again", droneSerialNumber));
        }

        // get loaded items records
        var manifest = manifestRepository.findByDrone(drone.get());
        if (manifest.isEmpty()) {
            throw new RecordNotFoundException(String.format(
                    "Drone - %s has no loaded medication items", droneSerialNumber));
        }

        List<ManifestMedications> medList = manifestMedicationRepository.findByManifest(manifest.get());
        List<Medication> medications = new ArrayList<>();
        medList.forEach(med -> {
            var medication = medicationRepository.findByCode(med.getMedication());
            if (medication.isEmpty()) {
                throw new RecordNotFoundException(String.format("Invalid medication code - %s", med.getMedication()));
            }
            medications.add(medication.get());
        });

        return new ResponseDTO<>(
                StatusCodes.SUCCESS,
                "successful",
                medications);
    }

    public ResponseDTO<?> getAvailableDronesForLoading() {
        return new ResponseDTO<>(
                StatusCodes.SUCCESS,
                "successful",
                droneRepository.findByState(DroneState.IDLE));
    }

    public ResponseDTO<?> getDroneBatteryLevel(String droneSerialNumber) {
        log.info("Drone Battery Level Request - {}", droneSerialNumber);
        // check if drone record exist
        var drone = droneRepository.findBySerialNumber(droneSerialNumber);
        if (drone.isEmpty()) {
            throw new RecordNotFoundException(String.format(
                    "No registered drone with serial number - %s. Register drone and try again", droneSerialNumber));
        }

        return new ResponseDTO<>(
                StatusCodes.SUCCESS,
                "successful",
                Arrays.asList(BatteryLevelDto.builder()
                        .batteryLevel(drone.get().getBatteryCapacity())
                        .build()));
    }

    @Scheduled(cron = "@hourly")
    public void periodicDroneBatteryLevelCheck() {
        log.info("INITIALIZING PERIODIC DRONE BATTERY LEVEL CHECK @{}", ZonedDateTime.now());
        List<Drone> drones = droneRepository.findAll();
        if (!drones.isEmpty()) {
            droneCount = 0;
            drones.forEach(drone -> {
                // check for battery level
                int droneBatteryLevel = drone.getBatteryCapacity();
                if (droneBatteryLevel < 25) {
                    // check if drone is not IDLE
                    if (!drone.getState().equals(DroneState.IDLE)) {
                        // update the drone status
                        updateDroneState(drone, DroneState.IDLE);
                        // create log
                        createAuditLog(drone, "Drone status updated to IDLE");
                        createAuditLog(drone, "Drone needs to be charged");
                    }
                }

                droneCount++;
            });
            log.info("TOTAL NUMBER OF DRONES CHECKED - {}", droneCount);
        }
        log.info("COMPLETED PERIODIC DRONE BATTERY LEVEL CHECK @{}", ZonedDateTime.now());
    }

    public ResponseDTO<?> getListOfMedications() {
        return new ResponseDTO<>(
                StatusCodes.SUCCESS,
                "Successful",
                medicationRepository.findAll());
    }

    private void createAuditLog(Drone drone, String action) {
        auditLogsRepository.save(AuditLogs.builder()
                .action(action)
                .drone(drone)
                .build());
    }

    private ModelTypes determineDroneModel(double weight) {
        ModelTypes model = null;
        if (weight <= 125) {
            model = ModelTypes.Lightweight;
        } else if (weight > 125 && weight <= 250) {
            model = ModelTypes.Middleweight;
        } else if (weight > 250 && weight <= 375) {
            model = ModelTypes.Cruiserweight;
        } else {
            model = ModelTypes.Heavyweight;
        }
        return model;
    }

    private String determineWeightLimit(ModelTypes type) {
        String weightLimit = "0g";
        if (type.equals(ModelTypes.Lightweight)) {
            weightLimit = "125gr";
        } else if (type.equals(ModelTypes.Middleweight)) {
            weightLimit = "250gr";
        } else if (type.equals(ModelTypes.Cruiserweight)) {
            weightLimit = "375gr";
        } else {
            weightLimit = "500gr";
        }
        return weightLimit;
    }

    private boolean checkDroneBatteryCapacityForLoading(int currentBatteryCapacity) {
        boolean isClearForLoading = Integer.valueOf(MINIMUM_BATTERY_LEVEL_FOR_LOADING) > currentBatteryCapacity
                ? false
                : true;
        return isClearForLoading;
    }

    private boolean checkDroneState(DroneState droneState) {
        boolean isIdle = droneState.equals(DroneState.IDLE) ? true : false;
        return isIdle;
    }

    private double calculateTotalMedicationWeight(List<MedicationDto> medications) {
        double totalWeight = 0;
        for (MedicationDto med : medications) {
            String medCode = med.getCode();
            var medication = medicationRepository.findByCode(medCode);
            if (medication.isEmpty()) {
                throw new RecordNotFoundException(String.format("Invalid medication code - %s", medCode));
            }
            // add the weight of medication to totalMedWeight
            totalWeight += medication.get().getWeight();
        }
        return totalWeight;
    }

    private List<String> getMedications(List<MedicationDto> medications) {
        List<String> meds = new ArrayList<>();
        medications.forEach(m -> {
            meds.add(m.getCode());
        });
        return meds;
    }

    private void updateDroneState(Drone drone, DroneState droneState) {
        drone.setState(droneState);
        droneRepository.save(drone);
        log.info("successfully updated drone state. New state for drone - {} is  {}", drone.getSerialNumber(),
                droneState);
    }
}
