# Drones

[[_TOC_]]

---

:scroll: **START**

## Introduction

> **Problem Statement:** There is a major new technology that is destined to be a disruptive force in the field of transportation: **the drone**. Just as the mobile phone allowed developing countries to leapfrog older technologies for personal communication, the drone has the potential to leapfrog traditional transportation infrastructure.Useful drone functions include delivery of small items that are (urgently) needed in locations with difficult access. We have a fleet of **10 drones**. A drone is capable of carrying devices, other than cameras, and capable of delivering small loads. For our use case **the load is medications**.

This is the solution to the drones problem statement. This solution is RESTful and exposes 6 API endpoints which allows

- registering a drone;
- loading a drone with medication items;
- checking loaded medication items for a given drone;
- checking available drones for loading;
- check drone battery level for a given drone;

and

- checking list of medications.

> Preloaded data set (```10 Drones``` & ```17 Medications```) in the `init.sql` file can be found in the **changelog** directory.
---
> A H2 in-memory database is used for this solution.

- H2 console available at '/h2-console'
- JDBC URL = `jdbc:h2:mem:local`
- user = `h2-user`
- password = `password`

---
> it is assumed that you have Java install on your machine.

After cloning, Follow the steps below to Build, Run and Test the application.

Using Maven, open the application root directory in the command line:

### Build

Navigate to the root directory of the application.

run
> ```mvn clean install```

### Run

navigate to the target folder in the root directory of the application.

run
> `java -jar drones-0.0.1-SNAPSHOT.jar`

To start the application on a different port other than the default port ```8590``` set, use the command below
> `java -jar drones-0.0.1-SNAPSHOT.jar --server.port={YOUR_CUSTOM_PORT_NUMBER}`

where `YOUR_CUSTOM_PORT_NUMBER` is a variable equal to the value of the desired port number you want to start the application on.

### Test

Here is it is assumed the application is started on the default port `8590`
> `baseUrl` = "http://localhost:8590"
---

#### Registering a Drone

> ENDPOINT: `"{baseUrl}/drone/register"`

>METHOD: **`POST`**

> SAMPLE REQUEST BODY:
`{
    "serialNumber": "{{$randomUUID}}",
    "weight": 330
}`

> SAMPLE RESPONSE: `{
    "statusCode": "0000",
    "statusMessage": "successfully registered drone",
    "data": [
        {
            "id": 11,
            "serialNumber": "3e3969ba-6305-49e3-a031-fc787e90253f",
            "model": "Cruiserweight",
            "weightLimit": "375gr",
            "batteryCapacity": 100,
            "state": "IDLE"
        }
    ],
    "errors": []
}`
---

#### loading a drone with medication items

> ENDPOINT: `"{baseUrl}/drone/meds/load"`

>METHOD: **`POST`**

> SAMPLE REQUEST BODY:
`{
    "droneSerialNumber": "39c720ff-360b-4a94-b17b-d558eec71fab",
    "medications" : [
        {
            "code": "KJHG456"
        },
        {
            "code": "RTE4577"
        },
        {
            "code": "XZCVB57IU"
        },
        {
            "code": "ZXCTVIU7"
        }
    ]
}`
---

#### checking loaded medication items for a given drone

> ENDPOINT: `"{baseUrl}/drone/{droneSerialNumber}/meds"`

> METHOD: **`GET`**

> PATH VARIABLE: _`droneSerialNumber`_, DataType - _`String`_

---

#### checking available drones for loading

> ENDPOINT: `"{baseUrl}/drones/list/loading"`

> METHOD: **`GET`**

---

#### check drone battery level for a given drone

> ENDPOINT: `"{baseUrl}/drone/{droneSerialNumber}/battery-level"`

> METHOD: **`GET`**

> PATH VARIABLE: _`droneSerialNumber`_, DataType - _`String`_

---

#### checking list of medications

>ENDPOINT: `"{baseUrl}/med/list"`

> METHOD: **`GET`**
---

There is an hourly periodic schedule task to check drones battery levels and create history/audit event log.

:scroll: **END**
